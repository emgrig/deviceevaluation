using DeviceEvaluationCoreService;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace DeviceEvaluation
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = Path.GetDirectoryName(System.Reflection
                  .Assembly.GetExecutingAssembly().CodeBase);
            Regex regex = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var logFilePath = Path.Combine(regex.Match(path).Value, "logFile.txt");
                        
            DeviceEvaluationService.EvaluateLogFile(logFilePath);
        }
    }
}
