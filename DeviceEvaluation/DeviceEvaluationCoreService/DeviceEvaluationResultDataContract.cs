﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeviceEvaluationCoreService
{
    public  class DeviceEvaluationResultDataContract
    {
        public  string DeviceName { get; set; }

        public  string DeviceStatus { get; set; }
    }
}
