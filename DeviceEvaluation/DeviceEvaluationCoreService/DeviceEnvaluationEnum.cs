﻿namespace DeviceEvaluationCoreService
{
    public enum DeviceTypeEnum
    {
        thermometer = 0,
        humidity = 1,
        monoxide = 2 // since the log has that name
    }

    public enum ThermometerStatus
    {
        UltraPrecise = 0,
        VeryPrecise = 1,
        Precise = 2
    }

    public enum HumidityStatus
    {
        Keep = 0,
        Discard = 1,
        //Retest = 2 potential status
    }

    public enum CarbonMonoxideStatus
    {
        // Yes, it's the same items as Humidity. I rather have them separated 
        Keep = 0,
        Discard = 1,
        //Retest = 2 potential status
    }
}
