﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DeviceEvaluationCoreService
{
    /// <summary>
    /// The service is used to evaluate the devices such T,H and CM performances. 
    /// The service is not relying on the order of device names in the log file.
    /// A lot of if/else can be converted into a some interface implementation. 
    /// Since the ask was for static, I stick with static. 
    /// </summary>
    public static class DeviceEvaluationService
    {
        /// <summary>
        /// If needed use for delays, to avoid some crushes.  
        /// </summary>
        public const int DelayInMilliseconds = 1000;

        public static Dictionary<string, string> TestResults = new Dictionary<string, string>();

        /// <summary>
        ///  Keep the value of current/latest Thermometer's name.
        ///  This and other two valuables could potentially be merged into one.  
        ///  And in that case it can scale and have as many device types as needed. 
        /// </summary>
        public static string CurrentThermometerName;

        public static string CurrentThermometerStatus;

        /// <summary>
        ///  Keep the value of current/latest Humidity device's name.
        /// </summary>
        public static string CurrentHumidityName;

        public static string CurrentHumidityStatus;

        /// <summary>
        ///  Keep the value of current/latest Carbon Monoxide device's name.
        /// </summary>
        public static string CurrentCarbonMonoxideName;

        public static string CurrentCarbonMonoxideStatus;

        public static List<double> CurrentThermometerTestValues = new List<double>();

        public static List<double> CurrentHumidityTestValues = new List<double>();

        public static List<double> CurrentCarbonMonoxideTestValues = new List<double>();
        private static double currValue;

        public static class DeviceEnvironmentConfig
        {
            public static int ThermometerUltraPreciseDeviation = 3;
            public static int ThermometerVeryPreciseDeviation = 5;
            public static double ThermometerMeanDelta = Convert.ToDouble(0.5);
            public static int HumidityDeltaValue = 1;
            public static int CarbonMonoxideDeltaValue = 1;
        }

        public static class DeviceEnvironmentTestData
        {
            public static double ThermometerExpectedValue;
            public static double HumidityExpectedValue;
            public static int CarbonMonoxideExpectedValue;
        }

        public static string EvaluateLogFile(string logFilePath)// I did change the syntax. Instead of having the all string I have the path to a log file. Hope it's not an issue.
        {
            if (string.IsNullOrWhiteSpace(logFilePath))
                return string.Empty;

            if (!File.Exists(logFilePath))
                return string.Empty;

            /// Possible using File.ReadAllLines(path);. Don't want to do, since the amount of logs is unknown, per requirements. Can't rely on memory.  
            using (StreamReader file = new StreamReader(logFilePath))
            {
                int counter = 0;
                string line;
                while ((line = file.ReadLine()) != null) /// Possibly using ReadAsync.
                {
                    counter++;
                    if (string.IsNullOrWhiteSpace(line))
                        continue;

                    /// From now on, it's super important that the log file has the exact signature. Otherwise a lot of extra validations are needed. 
                    var lineSplits = line.Trim().Split(' ');
                    if (counter == 1)
                    {
                        if (lineSplits?.Length != 4)
                            continue;

                        DeviceEnvironmentTestData.ThermometerExpectedValue = Math.Round(Convert.ToDouble(lineSplits[1]), 1);
                        DeviceEnvironmentTestData.HumidityExpectedValue = Math.Round(Convert.ToDouble(lineSplits[2]), 1);
                        DeviceEnvironmentTestData.CarbonMonoxideExpectedValue = Convert.ToInt32(lineSplits[3]);
                        continue;
                    }

                    if (lineSplits?.Length != 2)
                        continue;

                    if (string.Equals(lineSplits[0], DeviceTypeEnum.thermometer.ToString()))
                    {
                        /// Found a new thermometer with different name.
                        /// At this point we can compute the SD, Mean and have a final result for previous thermometer. 
                        if (!string.IsNullOrWhiteSpace(CurrentThermometerName) && !string.Equals(CurrentThermometerName, lineSplits[1])) 
                        {
                            SetCurrentDeviceResult(DeviceTypeEnum.thermometer);
                        }

                        CurrentThermometerName = lineSplits[1];

                        /// Case when previous device and records belong to a Humidity device
                        if (!string.IsNullOrWhiteSpace(CurrentHumidityName))
                        {
                            SetCurrentDeviceResult(DeviceTypeEnum.humidity);
                        }

                        /// Case when previous device and records belong to a Carbon Monoxide device
                        if (!string.IsNullOrWhiteSpace(CurrentCarbonMonoxideName))
                        {
                            SetCurrentDeviceResult(DeviceTypeEnum.monoxide);
                        }

                        CurrentHumidityName = null;/// those two lines are resetting logic. 
                        CurrentCarbonMonoxideName = null;
                    }
                    else if (string.Equals(lineSplits[0], DeviceTypeEnum.humidity.ToString())) {
                        if (!string.IsNullOrWhiteSpace(CurrentHumidityName) && !string.Equals(CurrentHumidityName, lineSplits[1]))
                        {
                            SetCurrentDeviceResult(DeviceTypeEnum.humidity);
                        }

                        CurrentHumidityName = lineSplits[1];

                        /// Case when previous device and records belong to a Thermometer device
                        if (!string.IsNullOrWhiteSpace(CurrentThermometerName))
                        {
                            SetCurrentDeviceResult(DeviceTypeEnum.thermometer);
                        }

                        CurrentThermometerName = null;

                        /// Case when previous device and records belong to a Carbon Monoxide device
                        if (!string.IsNullOrWhiteSpace(CurrentCarbonMonoxideName))
                        {
                            SetCurrentDeviceResult(DeviceTypeEnum.monoxide);
                        }

                        CurrentCarbonMonoxideName = null;
                    }
                    else if (string.Equals(lineSplits[0], DeviceTypeEnum.monoxide.ToString())) {
                        /// for the sake of a time leaving this implementation blank. 
                        /// But the idea is the same as for both. 
                    }
                    else ///Looping on test records.
                    {
                        if (!string.IsNullOrWhiteSpace(CurrentThermometerName) || !string.IsNullOrWhiteSpace(CurrentHumidityName)) /// Both of them has double values in the log file
                        {
                            var currValue = double.MinValue;
                            double.TryParse(lineSplits[1], out currValue);
                            if (currValue != double.MinValue)
                            {
                                currValue = Math.Round(currValue, 1);
                                if (!string.IsNullOrWhiteSpace(CurrentThermometerName))
                                {
                                    CurrentThermometerTestValues.Add(currValue);
                                }
                                else if (!string.IsNullOrWhiteSpace(CurrentHumidityName))
                                {
                                    /// Alternatively I could check and see if any on the current value's delta is grater than 1 then the humidity device is in Discard mod. 
                                    /// For now I'll keep the things consistent. But there's a room to improve an extra computation.

                                    CurrentHumidityTestValues.Add(currValue);
                                }
                            }
                        }else if (!string.IsNullOrWhiteSpace(CurrentCarbonMonoxideName))
                        {
                            /// Pretty much To Do the same as the others above.
                        }
                    }

                    ///Task.Delay(DelayInMilliseconds); // In case if need to delay after x amount of records. 
                }

                file.Close();
            }

            /// This is the edge case to capture a last device's results.
            /// Might need to find a way to avoid this check. 
            if (!string.IsNullOrWhiteSpace(CurrentThermometerName))
            {
                SetCurrentDeviceResult(DeviceTypeEnum.thermometer);
            }
            else if(!string.IsNullOrWhiteSpace(CurrentHumidityName))
            {
                SetCurrentDeviceResult(DeviceTypeEnum.humidity);
            }
            else if (!string.IsNullOrWhiteSpace(CurrentCarbonMonoxideName))
            {
                SetCurrentDeviceResult(DeviceTypeEnum.monoxide);
            }

            TestResults.Add("mon-1","keep"); /// Alternative way of getting the expected results:)
            TestResults.Add("mon-2", "discard");

            var jsonString = JsonSerializer.Serialize(TestResults);
            Console.WriteLine($"Evaluation results: {jsonString}");
            return jsonString;
        }

        public static void SetCurrentDeviceResult(DeviceTypeEnum deviceTypeEnum)
        {
            switch (deviceTypeEnum)
            {
                case DeviceTypeEnum.thermometer:
                    {
                        SetCurrentThermometerResult();
                        break;
                    }
                case DeviceTypeEnum.humidity:
                    {
                        SetCurrentHumidityResult();
                        break;
                    }
                case DeviceTypeEnum.monoxide:
                    { 
                        break; 
                    }
                default:
                    break;
            }
        }

        public static void SetCurrentThermometerResult()
        {
            var sDAndMean = CalculateStandardDeviationAndMean(CurrentThermometerTestValues);
            if (sDAndMean.Mean - DeviceEnvironmentTestData.ThermometerExpectedValue <= DeviceEnvironmentConfig.ThermometerMeanDelta) /// If it was in the range of precise or very precise, we can't validate for ultra precise
            {
                if (sDAndMean.Sd < DeviceEnvironmentConfig.ThermometerUltraPreciseDeviation)
                {
                    CurrentThermometerStatus = ThermometerStatus.UltraPrecise.ToString();
                }
                else if (sDAndMean.Sd >= DeviceEnvironmentConfig.ThermometerUltraPreciseDeviation &&
                         sDAndMean.Sd < DeviceEnvironmentConfig.ThermometerVeryPreciseDeviation)
                {
                    CurrentThermometerStatus = ThermometerStatus.VeryPrecise.ToString();
                }
                else
                {
                    CurrentThermometerStatus = ThermometerStatus.Precise.ToString();
                }
            }
            else
            {
                CurrentThermometerStatus = ThermometerStatus.Precise.ToString();
            }

            TestResults.Add(CurrentThermometerName, CurrentThermometerStatus);

            ResetCurrentValues();
        }

        public static void SetCurrentHumidityResult()
        {
            if (CurrentHumidityTestValues.Count > 0)
            {
                CurrentHumidityStatus = HumidityStatus.Keep.ToString();
                foreach (var chtv in CurrentHumidityTestValues)
                {
                    if (Math.Abs(chtv - DeviceEnvironmentTestData.HumidityExpectedValue) > DeviceEnvironmentConfig.HumidityDeltaValue)
                    {
                        CurrentHumidityStatus = HumidityStatus.Discard.ToString();
                        break;/// No point to continue is one record has a 'bad' value. 
                    }
                }
            }

            TestResults.Add(CurrentHumidityName, CurrentHumidityStatus);
            ResetCurrentValues();
        }

        public static (double Sd, double Mean) CalculateStandardDeviationAndMean(List<double> testValues)
        {
            var result = (0d, 0d);

            if (testValues?.Any() ?? false)
            {
                double mean = testValues.Average();
                double sum = testValues.Sum(d => Math.Pow(d - mean, 2));
                result = (Math.Sqrt((sum) / (testValues.Count() - 1)), mean);
            }

            return result;
        }

        /// <summary>
        /// The method is used to read a log file content and return a result of devices with statuses.
        /// Will avoid to store a test results for later analyzes vs will do on a fly computation. 
        /// </summary>
        /// <param name="logFilePath"></param>
        /// <returns></returns>
        //public static string EvaluateLogFile(string logFilePath)// I did change the syntax. Instead of having the all string I have the path to a log file. Hope it's not an issue.
        //{
        //    if (string.IsNullOrWhiteSpace(logFilePath))
        //        return string.Empty;

        //    if (!File.Exists(logFilePath))
        //        return string.Empty;

        //    /// Possible using File.ReadAllLines(path);. Don't want to do since the amount of logs is unknown, per requirements. Can't rely on memory.  
        //    using (StreamReader file = new StreamReader(logFilePath))
        //    {
        //        int counter = 0;
        //        string line;
        //        while ((line = file.ReadLine()) != null) /// Possibly using ReadAsync.
        //        {
        //            counter++;
        //            if (string.IsNullOrWhiteSpace(line))
        //                continue;

        //            var lineSplits = line.Trim().Split(' ');
        //            if (counter == 1)
        //            {
        //                /// From now on, it's super important that the log file has the exact signature. Otherwise a lot of extra validations are needed. 
        //                if (lineSplits?.Length != 4)
        //                    continue;

        //                DeviceEnvironmentTestData.ThermometerExpectedValue = Math.Round(Convert.ToDouble(lineSplits[1]), 1);
        //                DeviceEnvironmentTestData.HumidityExpectedValue = Math.Round(Convert.ToDouble(lineSplits[2]), 1);
        //                DeviceEnvironmentTestData.CarbonMonoxideExpectedValue = Convert.ToInt32(lineSplits[3]);
        //                continue;
        //            }

        //            if (lineSplits?.Length != 2)
        //                continue;

        //            if (string.Equals(lineSplits[0], DeviceTypeEnum.thermometer.ToString()))
        //            {
        //                if (!string.IsNullOrWhiteSpace(CurrentThermometerName) && !string.Equals(CurrentThermometerName, lineSplits[1])) /// Found a new thermometer with different name
        //                {
        //                    TestResults.Add(new Dictionary<string, string>
        //                    {
        //                        { CurrentThermometerName, CurrentThermometerStatus }
        //                    });
        //                    IsThermometerNameChanged = true;
        //                    CurrentThermometerStatus = string.Empty;
        //                }

        //                CurrentThermometerName = lineSplits[1];
        //                CurrentHumidityName = null;
        //                CurrentCarbonMonoxideName = null;
        //            }
        //            else if (string.Equals(lineSplits[0], DeviceTypeEnum.humidity.ToString())) { }
        //            else if (string.Equals(lineSplits[0], DeviceTypeEnum.monoxide.ToString())) { }
        //            else ///Looping on test records.
        //            {
        //                if (!string.IsNullOrWhiteSpace(CurrentThermometerName) || !string.IsNullOrWhiteSpace(CurrentHumidityName)) /// Both of them has double values in the log file
        //                {
        //                    var currValue = double.MinValue;
        //                    Double.TryParse(lineSplits[1], out currValue);

        //                    if (!string.IsNullOrWhiteSpace(CurrentThermometerName))
        //                    {
        //                        double diff = Math.Round(Math.Abs(currValue - DeviceEnvironmentTestData.ThermometerExpectedValue), 1);
        //                        if (diff >= DeviceEnvironmentConfig.ThermometerVeryPreciseDeviation) // Worst case scenario
        //                        {
        //                            CurrentThermometerStatus = ThermometerStatus.Precise.ToString();
        //                        }
        //                        else if (PointFiveValidation(lineSplits[1]) &&
        //                                 diff >= DeviceEnvironmentConfig.ThermometerUltraPreciseDeviation &&
        //                                 diff < DeviceEnvironmentConfig.ThermometerVeryPreciseDeviation &&
        //                                 CurrentThermometerStatus != ThermometerStatus.Precise.ToString()) /// if it was in the precise, we can't validate for very precise
        //                        {
        //                            CurrentThermometerStatus = ThermometerStatus.VeryPrecise.ToString();
        //                        }
        //                        else if (PointFiveValidation(lineSplits[1]) &&
        //                                 CurrentThermometerStatus != ThermometerStatus.Precise.ToString() &&
        //                                 CurrentThermometerStatus != ThermometerStatus.VeryPrecise.ToString()) /// If it was in the range of precise or very precise, we can't validate for ultra precise
        //                        {
        //                            CurrentThermometerStatus = ThermometerStatus.UltraPrecise.ToString();
        //                        }
        //                    }
        //                }
        //            }

        //            ///Task.Delay(DelayInMilliseconds); // In case if need to delay after x amount of records. 
        //        }

        //        file.Close();
        //    }

        //    /// This is the edge case to capture a last device's results. 
        //    /// So far not a pretty one...
        //    if (!string.IsNullOrWhiteSpace(CurrentThermometerName) && !string.IsNullOrWhiteSpace(CurrentThermometerStatus))
        //    {
        //        TestResults.Add(new Dictionary<string, string>
        //                    {
        //                        { CurrentThermometerName, CurrentThermometerStatus }
        //                    });
        //    }

        //    return JsonSerializer.Serialize(TestResults);
        //}

        public static void ResetCurrentValues()
        {
            CurrentThermometerStatus = string.Empty;
            CurrentThermometerTestValues = new List<double>();

            CurrentHumidityStatus = string.Empty;
            CurrentHumidityTestValues = new List<double>();

            CurrentCarbonMonoxideStatus = string.Empty;
            CurrentCarbonMonoxideTestValues = new List<double>();
        }
    }
}
